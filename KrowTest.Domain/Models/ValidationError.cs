﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KrowTest.Domain
{
    public class ValidationError
    {
        public ValidationError(int Line, ErrorType errType)
        {
            LineNumber = Line;
            _errType = errType;
        }

        public enum ErrorType
        {
            IncorrectNumberOfColumns,
            MissingAccount,
            MissingDescripton,
            MissingCurrencyCode,
            MissingAmount,
            InvalidCurrencyCode,
            InvalidCurrencyAmount
        }

        private ErrorType _errType;
        public int LineNumber { get; private set; }

        // todo: use text of enum to display error rather than this switch statement, will make it easier to add further error types.
        public string ErrorMessage
        {
            get
            {
                switch (this._errType)
                {
                    case ErrorType.IncorrectNumberOfColumns:
                        return string.Format("At line {0}: {1} ", LineNumber, "Incorrect number of columns");
                    case ErrorType.MissingAccount:
                        return string.Format("At line {0}: {1} ", LineNumber, "Missing account column");
                    case ErrorType.MissingAmount:
                        return string.Format("At line {0}: {1} ", LineNumber, "Missing amount column");
                    case ErrorType.MissingCurrencyCode:
                        return string.Format("At line {0}: {1} ", LineNumber, "Missing currency code column");
                    case ErrorType.MissingDescripton:
                        return string.Format("At line {0}: {1} ", LineNumber, "Missing description column");
                    case ErrorType.InvalidCurrencyAmount:
                        return string.Format("At line {0}: {1} ", LineNumber, "Currency Amount is invalid");
                    case ErrorType.InvalidCurrencyCode:
                        return string.Format("At line {0}: {1} ", LineNumber, "Currency code is invalid");
                }
                return string.Empty;
            }
        }
    }
}
