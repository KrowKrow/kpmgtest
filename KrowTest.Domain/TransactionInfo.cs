﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KrowTest.Data;
using KrowTest.Data.Models;

namespace KrowTest.Domain
{
    public class TransactionInfo
    {
        public List<ValidationError> ValidateAndInsert(int lineNumber, string line)
        {
            MapTransactionData ft = new MapTransactionData();

            TransactionData td = new TransactionData();

            if (ft.ToTransactionData(lineNumber, line, out td) == true)
            {
                InsertData(td);
            }
            else
            {
                return ft.ValidationErrors;
            }

            return null;
        }

        public void InsertData(TransactionData transactionData)
        {
            using (TransactionContext db = new Data.TransactionContext())
            {
                db.TransactionData.Add(transactionData);
                db.SaveChanges();
            };
        }

        public void ClearDB()
        {
            using (TransactionContext db = new Data.TransactionContext())
            {
                // would rather use this, but it ont connect to the db??
                //db.Database.ExecuteSqlCommand("truncate table transactiondatas");

                foreach (var tran in db.TransactionData)
                {
                    db.TransactionData.Remove(tran);
                }
                db.SaveChanges();
            };
        }

        public List<TransactionData> GetAllTransactionRows()
        {
            List<TransactionData> items = new List<TransactionData>();

            using (TransactionContext db = new Data.TransactionContext())
            {
                items = db.TransactionData.ToList();
            };

            return items;
        }

        public void UpdateRange(List<TransactionData> transactionDataList)
        {
            using (TransactionContext db = new Data.TransactionContext())
            {
                foreach (TransactionData tdItem in transactionDataList)
                {
                    var dbItem = db.TransactionData.Where(x => x.TransactionId == tdItem.TransactionId).FirstOrDefault();

                    if (dbItem != null)
                    {
                        dbItem.Account = tdItem.Account;
                        dbItem.Ammount = tdItem.Ammount;
                        dbItem.CurrencyCode = tdItem.CurrencyCode;
                        dbItem.Description = tdItem.Description;
                    }
                }

                db.SaveChanges();
            };
        }

        public void UpdateData(TransactionData transactionData)
        {
            throw new NotImplementedException();
        }

        public void DeleteData(TransactionData transactionData)
        {
            throw new NotImplementedException();
        }

    }
}
