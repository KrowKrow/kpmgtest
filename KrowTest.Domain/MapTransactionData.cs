﻿using KrowTest.Data.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KrowTest.Domain
{
    public class MapTransactionData
    {
        private const int EXPECTED_COLUMNS = 4;
        public List<ValidationError> ValidationErrors = new List<ValidationError>();

        private enum cols
        {
            Acc = 0,
            Desc = 1,
            CCode = 2,
            Amt = 3
        }
  
        public bool ToTransactionData(int lineNumber, string input, out TransactionData transactionData)
        {
            transactionData = new TransactionData();

            string[] columns = input.Split(',');

            if (columns.Count() == EXPECTED_COLUMNS)
            {
                if (string.IsNullOrWhiteSpace(columns[(int)cols.Acc].ToString()))
                {
                    ValidationErrors.Add(new ValidationError(lineNumber, ValidationError.ErrorType.MissingAccount));
                }
                else
                {
                    transactionData.Account = columns[(int)cols.Acc];
                }

                if (string.IsNullOrWhiteSpace(columns[(int)cols.Desc].ToString()))
                {
                    ValidationErrors.Add(new ValidationError(lineNumber, ValidationError.ErrorType.MissingDescripton));
                }
                else
                {
                    transactionData.Description = columns[(int)cols.Desc];
                }

                // check to see if the currency symbol exists in the culture-info list
                if( CultureInfo.GetCultures(CultureTypes.SpecificCultures)
                    .Where(x => (new RegionInfo(x.LCID)).ISOCurrencySymbol == columns[(int)cols.CCode].Trim().ToUpper()).Any())
                {
                    transactionData.CurrencyCode = columns[(int)cols.CCode];
                }
                else
                {
                    ValidationErrors.Add(new ValidationError(lineNumber, ValidationError.ErrorType.InvalidCurrencyCode));
                }

                decimal tempD = 0;
                if (decimal.TryParse(columns[(int)cols.Amt], out tempD))
                {
                    transactionData.Ammount = tempD;
                }
                else
                {
                    ValidationErrors.Add(new ValidationError(lineNumber, ValidationError.ErrorType.InvalidCurrencyAmount));
                }
            }
            else
            {
                ValidationErrors.Add(new ValidationError(lineNumber,ValidationError.ErrorType.IncorrectNumberOfColumns));
            }

            return (ValidationErrors.Count == 0);
        }
    }
}
