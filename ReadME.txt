Hello.

The site I have created will allow you to upload a csv.  It will validate each row.  Valid rows will be written to the database
and those that have issues will not.  Validation errors are kept in a model and displayed to the user.
-invalid columns
-missing columns
-invalid currency code
-invalid amount data-type

I have a folder named: TestUploadData, that contains a range of valid and invalid files.

After uploading, the site will then display the rows that actually made it into the database, in table on screen.  You can then update a value, hit 
update and this should then be reflected in the database.  It probably would have made more sense to display the rows that failed validation
so that the user could correct this, perhaps this is what you were aiming for. And I would like to have added this...

Things I would have added:
-IOC DI...(unity)
-Repository patttern
-More unit tests, especially around the validation
-Interfaces, which would have been required for the IOC
-For speed, I have some inline styles, these I would have transferred to CSS.
-Ensure only CSV is permitted

** I had alot of issues to begin with setting the correct connectionstring for EF, it works for the unit-tests but refused to read the config
for the web-project.  Eventually I just hard-coded the connectionstring in the DBContext, so you will need to alter that rather than the web.config
sorry about that.


Many thanks, Kieron Row

    