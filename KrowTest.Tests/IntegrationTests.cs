﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using KrowTest.Data;

namespace KrowTest.Tests
{
    [TestClass]
    public class IntegrationTests
    {
        [TestMethod]
        public void Data_InsertData()
        {
            using (TransactionContext db = new Data.TransactionContext())
            {
                var RowCountBefore = db.TransactionData.Count();

                db.TransactionData.Add(new Data.Models.TransactionData() { Account = "test" });
                db.SaveChanges();

                var RowCountAfter = db.TransactionData.Count();

                Assert.AreEqual(RowCountBefore + 1, RowCountAfter);
            };
        }

        [TestMethod]
        public void Domain_GetAllRows()
        {
            using (TransactionContext db = new Data.TransactionContext())
            {
                var ExpectedRowCount = db.TransactionData.Count();

                Domain.TransactionInfo ti = new Domain.TransactionInfo();

                var ActualCount = ti.GetAllTransactionRows().Count();

                Assert.AreEqual(ExpectedRowCount, ActualCount);
            };
        }


        [TestMethod]
        public void Domain_Insert()
        {
            using (TransactionContext db = new Data.TransactionContext())
            {
                var BeforeCount = db.TransactionData.Count();

                Domain.TransactionInfo ti = new Domain.TransactionInfo();

                ti.InsertData(new Data.Models.TransactionData() { Account = "Test" });

                var AfterCount = db.TransactionData.Count();

                Assert.AreEqual(BeforeCount + 1, AfterCount);
            };
        }
    }
}
