﻿using KrowTest.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KrowTest.Data
{
    public class TransactionContext : DbContext
    {
        public TransactionContext() : base(@"data source=USER-PC\;initial catalog=krowTestKPMG;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework")
        {
        }

        public DbSet<TransactionData> TransactionData { get; set; }
    }
}
