﻿using KrowTest.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KrowTest.Web.Models
{
    public class TransactionsViewModel
    {
        public List<ValidationError> valdationErrors = new List<ValidationError>();
        public List<TransactionDataViewModel> UploadedData = new List<TransactionDataViewModel>();
    }
}