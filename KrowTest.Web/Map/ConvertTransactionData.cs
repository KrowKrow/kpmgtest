﻿using KrowTest.Data.Models;
using KrowTest.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KrowTest.Web.Map
{
    public class ConvertTransactionData
    {
        public List<TransactionDataViewModel> ToViewModel(List<TransactionData> transactionData)
        {
            List<TransactionDataViewModel> returnList = new List<TransactionDataViewModel>();

            foreach (TransactionData transactionDataItem in transactionData)
            {
                returnList.Add(new TransactionDataViewModel()
                {

                    Account = transactionDataItem.Account,
                    CurrencyCode = transactionDataItem.CurrencyCode,
                    Description = transactionDataItem.Description,
                    Ammount = transactionDataItem.Ammount,
                    TransactionId = transactionDataItem.TransactionId
                });
            };

            return returnList;
        }
        public List<TransactionData> ToModel(List<TransactionDataViewModel> transactionData)
        {
            List<TransactionData> returnList = new List<TransactionData>();

            foreach (TransactionDataViewModel transactionDataItemVM in transactionData)
            {
                returnList.Add(new TransactionData()
                {

                    Account = transactionDataItemVM.Account,
                    CurrencyCode = transactionDataItemVM.CurrencyCode,
                    Description = transactionDataItemVM.Description,
                    Ammount = transactionDataItemVM.Ammount,
                    TransactionId = transactionDataItemVM.TransactionId
                });
            };

            return returnList;
        }

    }
}