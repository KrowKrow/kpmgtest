﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KrowTest.Domain;
using KrowTest.Web.Models;
using KrowTest.Data.Models;
using KrowTest.Web.Map;

namespace KrowTest.Web.Controllers
{
    public class UploadController : Controller
    {
        // GET: Upload
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ActionName("Index")]
        public ActionResult IndexPost()
        {
            int UploadedCounter = 0;
            int TotalRows = 0;
            int LineNumber = 1;

            // domain object that will do all the work...
            TransactionInfo transactionInfo = new TransactionInfo();

            // remove all previous uploaded data
            transactionInfo.ClearDB();

            // store of all errors returned from the domain 
            List<ValidationError> valdationErrors = new List<ValidationError>();
            
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
  
                // loop throuh each line of the file...
                using (var reader = new StreamReader(file.InputStream))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();

                        // validate the line, if ok, insert else return a list of errors
                        var err = transactionInfo.ValidateAndInsert(LineNumber, line);

                        if (err != null)
                        {
                            valdationErrors.AddRange(err);
                        }
                        else
                        {
                            UploadedCounter++;
                        }

                        // increment line counter, used in the error list to determine where the fault is.
                        LineNumber++;
                        TotalRows++;
                    }
                }
            }

            // get a fresh list of the data from the db, not really needed but wanted show db interaction
            List<TransactionData> trans = transactionInfo.GetAllTransactionRows();

            // build-up a view-model
            TransactionsViewModel transactionsViewModel = new TransactionsViewModel();
            // store validation errors
            transactionsViewModel.valdationErrors = valdationErrors;
            // store uploaded rows           
            transactionsViewModel.UploadedData = new ConvertTransactionData().ToViewModel(trans);

            ViewBag.State = "uploaded";
            ViewBag.Uploaded = UploadedCounter;
            ViewBag.TotalRows = TotalRows;
            // return the list of errors to display on screen
            return View(transactionsViewModel);
        }

        [HttpPost]
        public ActionResult Update(List<TransactionDataViewModel> UploadedData)
        {
            // create domain object that will do all the work...
            TransactionInfo transactionInfo = new TransactionInfo();

            // map the view-model back to the model
            List<TransactionData> transactionDataList = new ConvertTransactionData().ToModel(UploadedData);

            transactionInfo.UpdateRange(transactionDataList);

            return View();
        }
    }
}